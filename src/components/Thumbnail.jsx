import React from 'react'
import PropTypes from 'prop-types'

function Thumbnail(props) {
  return (
    <figure className="thumbnail">
      <img src={`https://in.bmscdn.com/events/moviecard/${props.EventGroup}.jpg`} alt={props.EventTitle}/>
      <figcaption>{props.EventTitle}</figcaption>
    </figure>
  )
}

Thumbnail.propTypes = {
  EventGroup: PropTypes.string,
  EventTitle: PropTypes.string
}

export default Thumbnail


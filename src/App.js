import React, { Component } from 'react';
import axios from 'axios';
import './App.css';

export default class App extends Component {

  constructor(props){
    super(props);
    this.state= {trailers: {}};
  }

  componentDidMount(){
    axios.get('https://in.bookmyshow.com/serv/getData?cmd=GETTRAILERS&mtype=cs')
    .then(function (response) {
      // handle success
      console.log(response);
    })
    .catch(function (error) {
      // handle error
      console.log(error);
    })
  }

  render() {
    return (
      <div className="App">
        
      </div>
    )
  }
}
